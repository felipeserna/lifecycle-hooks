import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit, OnDestroy, OnChanges {

  counter = 0;
  interval: any;

  @Input()
  channelName = "";

  constructor() {
    console.log("Child Constructor is called");
   }

  ngOnInit(): void {
    console.log("Child OnInit is called");

    // Comment below paragraph
    /*this.interval = setInterval(() => {
      this.counter = this.counter + 1;
      console.log(this.counter);
    }, 1000);*/
  }

  ngOnDestroy() {
    // Comment below line
    //clearInterval(this.interval);
    console.log("Child OnDestroy is called");
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
    console.log("Child OnChanges is called");
  }  

}
